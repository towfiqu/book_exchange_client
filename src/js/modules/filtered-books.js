import axios from 'axios';
const api = require('./config')();
const filtered_books = JSON.parse(localStorage.getItem('filtered_books'));
const filteredBooksWrapper = document.querySelector('.filtered-books-wrapper');
const genreHead = document.querySelector('.headline__genre');

function renderFilteredBooks() {
  genreHead.innerHTML = `${filtered_books[0].genre} Books`;
  if (filtered_books) {
    filteredBooksWrapper.innerHTML =
      filtered_books &&
      filtered_books
        .map(
          book => `<img
          class="book-cover"
          src=${book.cover_pic}
          data-bookid=${book.id}
          data-userid=${book.user_id}
          alt=""
        />`,
        )
        .join('');
  } else {
    filteredBooksWrapper.innerHTML = `<h2 style="color: firebrick;" class='headline'>No books Found</h2>`;
  }
}

renderFilteredBooks();

const bookCovers = document.querySelectorAll('.book-cover');

bookCovers.forEach(book => {
  book.addEventListener('click', function() {
    getBookDetails(this.dataset.bookid);
  });
});

function getBookDetails(bookid) {
  const config = {
    headers: { 'Content-Type': 'application/json' },
  };
  const data = {
    book_id: bookid,
  };
  axios
    .post(`${api}/api/books/get_book_details.php`, config, { data })
    .then(res => {
      localStorage.setItem('book_details', JSON.stringify(res.data));
      window.location.replace('book-details.html');
    });
}
