import axios from 'axios';
import Spinner from '../../assets/loading.svg';

const api = require('./config')();

const bookCardWrapper = document.querySelector('.book-card-wrapper');

let my_books = [];
let loading = false;

const user = JSON.parse(localStorage.getItem('logged_in_user'));
const userID = user.id;

const data = {
  user_id: userID,
};

const config = {
  headers: {
    'Content-Type': 'application/json',
  },
};

function getSingleUserBooks() {
  loading = true;
  if (loading) {
    showLoader(true);
  }
  setTimeout(() => {
    axios
      .post(`${api}/api/books/get_single_user_books.php`, config, { data })
      .then(res => {
        loading = false;
        my_books = res.data.data;
        bookCardWrapper.innerHTML = my_books
          ? my_books
              .map(
                book => ` <div class="book-card">
            <img
              id="book-cover"
              style="width: 120px; height: 160px;"
              src=${book.cover_pic}
              alt=""
            />
            <div class="book-details">
              <h3 id="book-title" style="align-self: end;">${book.title}</h3>
              <p id="book-author" style="font-size: 14px; ">${book.author}</p>
              <p id="book-genre" style="font-size: 14px; align-self: start;">
                ${book.genre}
              </p>
              ${
                book.collection == 'yes'
                  ? `<p id="book-cat" style="color: #00B087; align-self: start;">
                In Collection
              </p>`
                  : `<p id="book-cat" style="color: #123DAF; align-self: start;">
                In Want To Read
              </p>`
              }
              <button data-bookid=${book.id} class="book-del" >Delete</button>
            </div>
          </div>`,
              )
              .join('')
          : `<h1 style="color: crimson; margin-top: 20%;" class="headline">No books in library</h1>`;
        const bookDel = document.querySelectorAll('.book-del');
        bookDel.forEach(book => {
          book.addEventListener('click', function() {
            deleteBook(this.dataset.bookid);
          });
        });
      });
  }, 1000);
}

getSingleUserBooks();

function showLoader(loading) {
  if (loading) {
    bookCardWrapper.innerHTML = `
    <img  src=${Spinner} >
    `;
  }
}

function deleteBook(id) {
  const data = {
    book_id: id,
  };

  axios
    .delete(
      `${api}/api/books/delete_book.php`,

      { data },
    )
    .then(res => {
      if (res.data.message === 'Book Deleted') {
        showAlert('Book Deleted');
      }
    });
}

function showAlert(msg) {
  const showAlert = document.querySelector('.show-alert');

  if (msg === 'Book Deleted') {
    showAlert.innerHTML = `
          <p>Book Deleted Successfully!!</p>    
        `;
  }

  setTimeout(() => {
    showAlert.innerHTML = '<div class="show-alert"></div>';
    getSingleUserBooks();
  }, 1500);
}
