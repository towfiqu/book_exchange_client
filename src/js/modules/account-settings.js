import axios from 'axios';
const api = require('./config')();
// files for preparing necessary data for updating account

// Cloudinary Configs
const CLOUDINARY_URL = 'https://api.cloudinary.com/v1_1/towfiqu/image/upload';
const CLOUDINARY_PRESET = 'book_exchange';

// DOM Selection
const imageUpload = document.getElementById('image-input');
const imageLabel = document.getElementById('image-label');
const imagePreview = document.getElementById('image-preview');
const updateForm = document.getElementById('account-update-form');

// setting prpfile pic from db
const user = JSON.parse(localStorage.getItem('logged_in_user'));
const pic = user.profile_pic;
if (pic === 'profile_pic_url') {
  imagePreview.src =
    'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png';
} else {
  imagePreview.src = pic;
}

// Global Variables
let profilePic;

// setting up user input to current values
username.value = user.username;
email.value = user.email;
password.value = user.password;
profilePic = user.profile_pic;

// Image upload to cloudinary + preparing the image for database add.
imageUpload.addEventListener('change', addProfileImage);

function addProfileImage(e) {
  let loading = true;
  const image = e.target.files[0];
  imageLabel.innerHTML = image.name;

  const formData = new FormData();
  formData.append('file', image);
  formData.append('upload_preset', CLOUDINARY_PRESET);
  if (loading) {
    imagePreview.src =
      'https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif';
  }
  fetch(CLOUDINARY_URL, {
    method: 'POST',
    body: formData,
  }).then(res => {
    loading = false;
    res.json().then(res2 => {
      imagePreview.src = res2.secure_url;
      profilePic = res2.secure_url;
    });
  });
}

updateForm.onsubmit = e => {
  e.preventDefault();
  updateAccount();
};

function updateAccount() {
  const user = JSON.parse(localStorage.getItem('logged_in_user'));
  const userID = user.id;
  const data = {
    id: userID,
    username: username.value,
    email: email.value,
    password: password.value,
    profile_pic: profilePic,
  };
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  axios.put(`${api}/api/users/update_user.php`, config, { data }).then(res => {
    if (res.data.message === 'User Updated') {
      imagePreview.src = profilePic;
      showAlert('User Updated');
    }
  });
}

function showAlert(msg) {
  const showAlert = document.querySelector('.show-alert');

  if (msg === 'User Updated') {
    showAlert.classList.add('show-alert__success');
  }
  showAlert.innerHTML = `
      <p>Account Updated Successfully!! You need to login</p>    
    `;

  setTimeout(() => {
    localStorage.removeItem('logged_in_user');
    window.location.replace('login.html');
    showAlert.innerHTML = '<div class="show-alert"></div>';
  }, 1500);
}
