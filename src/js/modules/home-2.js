import axios from 'axios';
import Spinner from '../../assets/loading.svg';

const api = require('./config')();

const booksWrapper = document.querySelector('.books-wrapper');

let books = [];
let loading = false;

function getAllBooks() {
  loading = true;
  if (loading) {
    showLoader(loading);
  }

  setTimeout(() => {
    axios.get(`${api}/api/books/get_all_books.php`).then(res => {
      loading = false;
      books = res.data.data;
      booksWrapper.innerHTML =
        books &&
        books
          .map(
            book => `<img
        class="book-cover"
        src=${book.cover_pic}
        data-bookid=${book.id}
        data-userid=${book.user_id}
        alt=""
      />`,
          )
          .join('');

      const bookCovers = document.querySelectorAll('.book-cover');

      bookCovers.forEach(book => {
        book.addEventListener('click', function() {
          getBookDetails(this.dataset.bookid);
        });
      });
    });
  }, 1000);
}

getAllBooks();

function showLoader(loading) {
  if (loading) {
    booksWrapper.innerHTML = `
    <img style="grid-column: span 3; justify-self: center" src=${Spinner} >
    `;
  }
}

function getBookDetails(bookid) {
  const config = {
    headers: { 'Content-Type': 'application/json' },
  };
  const data = {
    book_id: bookid,
  };
  axios
    .post(`${api}/api/books/get_book_details.php`, config, { data })
    .then(res => {
      localStorage.setItem('book_details', JSON.stringify(res.data));
      window.location.replace('book-details.html');
    });
}
