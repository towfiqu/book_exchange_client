import axios from 'axios';
const api = require('./config')();
const logged_in_user = JSON.parse(localStorage.getItem('logged_in_user'));

let all_conv = [];
let unique_user_id = [];
let users = [];
let chats = [];

const config = {
  headers: {
    'Content-Type': 'application/json',
  },
};
const data = {
  user_id: logged_in_user.id,
};

axios
  .post(`${api}/api/messages/get_all_messages.php`, config, { data })
  .then(res => {
    all_conv = res.data;

    all_conv.map(conv => {
      if (
        conv.sender_id !== logged_in_user.id &&
        !unique_user_id.includes(conv.sender_id)
      ) {
        unique_user_id.push(conv.sender_id);
      }
      if (
        conv.receiver_id !== logged_in_user.id &&
        !unique_user_id.includes(conv.receiver_id)
      ) {
        unique_user_id.push(conv.receiver_id);
      }
    });
    localStorage.setItem('open_chat_user', unique_user_id[0]);

    getUser(unique_user_id);
  });

function getUser(unique_user_id) {
  unique_user_id.forEach(id => {
    const data = {
      user_id: id,
    };
    axios
      .post(`${api}/api/users/get_user_by_id.php`, config, { data })
      .then(res => {
        users.push(res.data);
        const userLists = document.querySelector('.mobile-user-lists');
        userLists.innerHTML =
          users &&
          users
            .map(
              user => `
    <div data-userid=${user.id} class="mobile-user-info">
          <img
            
            src=${user.profile_pic}
            alt=""
          />
          
            <p  >${user.username}</p>
           
          
        </div>
    `,
            )
            .join('');
        const listOfChats = document.querySelectorAll('.mobile-user-info');
        listOfChats[0].classList.add('mobile-active-user');
        listOfChats.forEach(chatUser => {
          chatUser.addEventListener('click', function() {
            loadMessages(this.dataset.userid, logged_in_user.id);
            localStorage.setItem('open_chat_user', this.dataset.userid);
            listOfChats.forEach(list => {
              if (list.classList.contains('mobile-active-user')) {
                list.classList.remove('mobile-active-user');
              }
            });
            chatUser.classList.add('mobile-active-user');
          });
        });
      });
  });
}

function loadChats() {
  const openConversation = document.querySelector('.mobile-open-chat');

  openConversation.innerHTML =
    chats &&
    chats
      .map(chat =>
        chat.username === logged_in_user.username
          ? `
<div class="mobile-self"><span>${chat.messages}</span></div>
`
          : `<div class="mobile-user"><span>${chat.messages}</span></div>`,
      )
      .join('');

  openConversation.scrollTop = openConversation.scrollHeight; //showing latest message by default
}

function loadMessages(id_1, id_2) {
  const data = {
    sender_id: id_1,
    receiver_id: id_2,
  };

  axios
    .post(`${api}/api/messages/get_individual_chats.php`, config, { data })
    .then(res => {
      chats = res.data;
      loadChats();
    });
}

loadMessages(localStorage.getItem('open_chat_user'), logged_in_user.id);

const replyBtn = document.querySelector('.mobile-send-reply');
const replyText = document.getElementById('mobile-chat-reply');
replyBtn.addEventListener('click', function() {
  sendMessage(replyText.value);
});

function sendMessage(msg) {
  const data = {
    msg: msg,
    username: logged_in_user.username,
    email: logged_in_user.email,
    profile_pic: logged_in_user.profile_pic,
    sender_id: logged_in_user.id,
    receiver_id: localStorage.getItem('open_chat_user'),
  };
  axios
    .post(`${api}/api/messages/send_message.php`, config, { data })
    .then(res => {
      replyText.value = '';
      loadMessages(localStorage.getItem('open_chat_user'), logged_in_user.id);
    });
}
