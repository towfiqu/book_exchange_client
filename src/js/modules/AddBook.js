import axios from 'axios';
const api = require('./config');
// Class for adding book data to database
// this file will only load after adding a book through add_books.js form.
class AddBook {
  constructor(coverImage, books_count) {
    this.events();
    this.api = api();
    // accepting coverImage from add_books.js
    this.coverImage = coverImage;
    // accepting books count to update books count after adding to db
    this.books_count = books_count;
  }

  events() {
    // add books form
    const addBooksForm = document.getElementById('add-books');
    addBooksForm.onsubmit = e => {
      e.preventDefault();
      this.addBook();
    };
  }

  // this will add a single book to database
  addBook() {
    let add_to_collection;
    let add_to_want_to_read;

    const user = JSON.parse(localStorage.getItem('logged_in_user'));
    const userID = user.id;

    if (add_to_col.checked) {
      add_to_collection = 'yes';
      add_to_want_to_read = 'no';
    }
    if (add_to_wtr.checked) {
      add_to_want_to_read = 'yes';
      add_to_collection = 'no';
    }
    const data = {
      title: title.value,
      author: author.value,
      genre: genre.value,
      details: details.value,
      cover_pic: this.coverImage,
      collection: add_to_collection,
      want_to_read: add_to_want_to_read,
      user_id: userID,
    };

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    axios
      .post(`${this.api}/api/books/add_book.php`, config, { data })
      .then(res => {
        if (res.data.message == 'Book Added') {
          this.showAlert('Book Added');
          this.emptyInputValues();
          this.books_count();
        } else {
          console.log('Something went wrong');
        }
      });
  }

  emptyInputValues() {
    title.value = '';
    author.value = '';
    genre.value = '';
    details.value = '';
    const imagePreview = document.getElementById('image-preview');
    imagePreview.src =
      'https://www.toddbershaddvm.com/wp-content/uploads/sites/257/2018/09/placeholder-img.jpg';
    const imageLabel = document.getElementById('image-label');
    imageLabel.innerHTML = 'Add Book Cover';
    add_to_col.checked = true;
    add_to_wtr.checked = false;
  }

  showAlert(msg) {
    const showAlert = document.querySelector('.show-alert');

    if (msg === 'Book Added') {
      showAlert.classList.add('show-alert__success');
    }
    showAlert.innerHTML = `
      <p>Book Added Successfully!!</p>    
    `;

    setTimeout(() => {
      showAlert.innerHTML = '<div class="show-alert"></div>';
    }, 1500);
  }
}

export default AddBook;
