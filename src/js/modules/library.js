import axios from 'axios';

import Spinner from '../../assets/loading.svg';
const api = require('./config')();
const bookCardWrapper = document.querySelector('.book-card-wrapper');
const userName = document.getElementById('user-name');

const bookData = JSON.parse(localStorage.getItem('book_details'));
let books = [];
let loading = false;

const config = {
  headers: {
    'Content-Type': 'application/json',
  },
};

const user_id = bookData.user_id;

const data = {
  user_id: user_id,
};

// get book owner's info

axios
  .post(`${api}/api/books/get_owner_info.php`, config, { data })
  .then(res => {
    userName.innerHTML = `${res.data.username}'s Library`;
  });

// get book owner's info end
function getSingleUserBooks() {
  loading = true;
  if (loading) {
    showLoader(loading);
  }
  setTimeout(() => {
    axios
      .post(`${api}/api/books/get_single_user_books.php`, config, { data })
      .then(res => {
        loading = false;
        books = res.data.data;
        bookCardWrapper.innerHTML = books
          ? books
              .map(
                book => ` <div data-bookid=${book.id} class="book-card">
            <img
              id="book-cover"
              style="width: 120px; height: 160px;"
              src=${book.cover_pic}
              alt=""
            />
            <div class="book-details">
              <h3 id="book-title" style="align-self: end;">${book.title}</h3>
              <p id="book-author" style="font-size: 14px; ">${book.author}</p>
              <p id="book-genre" style="font-size: 14px; align-self: start;">
                ${book.genre}
              </p>
              ${
                book.collection == 'yes'
                  ? `<p id="book-cat" style="color: #00B087; align-self: start;">
                In Collection
              </p>`
                  : `<p id="book-cat" style="color: #123DAF; align-self: start;">
                In Want To Read
              </p>`
              }
              
            </div>
          </div>`,
              )
              .join('')
          : `<h1 style="color: crimson; margin-top: 20%;" class="headline">No books in library</h1>`;
        const bookCard = document.querySelectorAll('.book-card');

        bookCard.forEach(book => {
          book.addEventListener('click', function() {
            getBookDetails(this.dataset.bookid);
          });
        });
      });
  }, 1000);
}

getSingleUserBooks();

function showLoader(loading) {
  if (loading) {
    bookCardWrapper.innerHTML = `
    <img  src=${Spinner} >
    `;
  }
}

function getBookDetails(bookid) {
  const config = {
    headers: { 'Content-Type': 'application/json' },
  };
  const data = {
    book_id: bookid,
  };
  axios
    .post(`${api}/api/books/get_book_details.php`, config, { data })
    .then(res => {
      localStorage.setItem('book_details', JSON.stringify(res.data));
      window.location.replace('book-details.html');
    });
}
