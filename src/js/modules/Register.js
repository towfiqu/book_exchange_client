import axios from 'axios';
const api = require('./config');
class Register {
  constructor() {
    this.events();
    this.api = api();
  }

  events() {
    const registerForm = document.getElementById('register-form');

    registerForm.onsubmit = e => {
      e.preventDefault();
      this.createUser();
    };
  }

  createUser() {
    const data = {
      username: username.value,
      email: email.value,
      password: password.value,
    };

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    axios
      .post(
        `${api}/api/users/create_user.php`,
        config,

        { data },
      )
      .then(res => {
        if (res.data.message === 'User already exists') {
          this.showAlert('User already exists');
        } else {
          this.showAlert('Registration successful. You can login now');
          username.value = '';
          email.value = '';
          password.value = '';
        }
      });
  }

  showAlert(msg) {
    const showAlert = document.querySelector('.show-alert');

    if (msg === 'Registration successful. You can login now') {
      showAlert.classList.add('show-alert__success');
    }
    showAlert.innerHTML = `
      <p>${msg}</p>    
    `;

    setTimeout(() => {
      showAlert.innerHTML = '<div class="show-alert"></div>';
    }, 3000);

    if (msg === 'Registration successful. You can login now') {
      setTimeout(() => {
        window.location.replace('login.html');
      }, 1500);
    }
  }
}

const register = new Register();

// export default Register;
