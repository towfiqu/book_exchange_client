import AddBook from './AddBook';
import axios from 'axios';

const api = require('./config')();

// files for preparing necessary data for adding to database
// this files will load immediately after opening add_books.html

// Cloudinary Configs
const CLOUDINARY_URL = 'https://api.cloudinary.com/v1_1/towfiqu/image/upload';
const CLOUDINARY_PRESET = 'book_exchange';

// DOM Selection
const imageUpload = document.getElementById('image-input');
const imageLabel = document.getElementById('image-label');
const imagePreview = document.getElementById('image-preview');

const booksCount = document.querySelector('.support-info');

// Global Variables
let coverImage;
let books_in_col;

// books count function later passed to AddBook Class since named function
const books_count = function getBooksCountInCol() {
  const user = JSON.parse(localStorage.getItem('logged_in_user'));
  const userID = user.id;

  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const data = {
    user_id: userID,
  };

  axios
    .post(`${api}/api/books/get_books_count_in_lib.php`, config, {
      data,
    })
    .then(res => {
      books_in_col = res.data.books_count;
      showBooksCount(books_in_col);
    });
};
books_count();

// This will update books count on ui dynamically
function showBooksCount(count) {
  if (parseInt(count) === 1) {
    booksCount.innerHTML = `${count} Book in Library`;
  } else {
    booksCount.innerHTML = `${count} Books in Library`;
  }
}

// Image upload to cloudinary + preparing the image for database add.
imageUpload.addEventListener('change', addBookImage);

function addBookImage(e) {
  let loading = true;
  const image = e.target.files[0];
  imageLabel.innerHTML = image.name;

  const formData = new FormData();
  formData.append('file', image);
  formData.append('upload_preset', CLOUDINARY_PRESET);
  if (loading) {
    imagePreview.src =
      'https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif';
  }
  fetch(CLOUDINARY_URL, {
    method: 'POST',
    body: formData,
  }).then(res => {
    loading = false;
    res.json().then(res2 => {
      imagePreview.src = res2.secure_url;
      coverImage = res2.secure_url;
      // constructing addbook after uploading image
      const addBook = new AddBook(coverImage, books_count);
    });
  });
}
