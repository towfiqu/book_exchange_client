import axios from 'axios';
const api = require('./config')();

const user = JSON.parse(localStorage.getItem('logged_in_user'));
if (!user) {
  window.location.replace('login.html');
}

const searchResults = document.querySelector('.search-results');
const searchResults2 = document.querySelector('.search-results-2');

let filtered_books = [];
let all_genres = [];

const searchField = document.getElementById('search-field');
const responsiverSearch = document.getElementById('responsive-search-field');
function performSearch(e) {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const data = {
    title: e.target.value,
  };

  axios
    .post(`${api}/api/books/search_books.php`, config, {
      data,
    })
    .then(res => {
      searchResults.innerHTML = `
       <img
              
              style="width: 50px;height: 50px; padding: 5px;"
              src=${res.data[0].cover_pic}
              alt=""
            />
            <div id='get-book' style="cursor: pointer; padding: 5px;" data-bookid=${res.data[0].id}>
              <p style="font-size: 18px; margin-bottom: 5px; ">${res.data[0].title}</p>
              <p style="font-size: 14px;">${res.data[0].author}</p>
            </div>
      `;

      setTimeout(() => {
        searchResults.classList.add('hide-search-results');
      }, 5000);

      const book = document.getElementById('get-book');
      book.addEventListener('click', function() {
        getBookDetails(this.dataset.bookid);
      });
    })
    .catch(err => {
      if (err) {
        searchResults.innerHTML = `<div class='search-results' ></div>`;
      }
    });
}
function performSearch2(e) {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const data = {
    title: e.target.value,
  };
  axios
    .post(`${api}/api/books/search_books.php`, config, { data })
    .then(res => {
      searchResults2.innerHTML = `
       <img
              
              style="width: 50px;height: 50px; padding: 5px;"
              src=${res.data[0].cover_pic}
              alt=""
            />
            <div id='get-book' style="cursor: pointer; padding: 5px;" data-bookid=${res.data[0].id}>
              <p style="font-size: 18px; margin-bottom: 5px; ">${res.data[0].title}</p>
              <p style="font-size: 14px;">${res.data[0].author}</p>
            </div>
      `;

      setTimeout(() => {
        searchResults2.classList.add('hide-search-results');
      }, 5000);

      const book = document.getElementById('get-book');
      book.addEventListener('click', function() {
        getBookDetails(this.dataset.bookid);
      });
    })
    .catch(err => {
      if (err) {
        searchResults.innerHTML = `<div class='search-results' ></div>`;
      }
    });
}
searchField.addEventListener('input', performSearch);
responsiverSearch.addEventListener('input', performSearch2);

function getAllgenres() {
  axios.get(`${api}/api/books/get_all_genres.php`).then(res => {
    all_genres = res.data;
    const list_genres = document.querySelector('.list-genres');
    list_genres.innerHTML =
      all_genres &&
      all_genres
        .map(
          genre =>
            `
          <span class="single-genre" >${genre}</span>
        `,
        )
        .join('');
    const genres = document.querySelectorAll('.single-genre');
    genres.forEach(genre => {
      genre.addEventListener('click', function(e) {
        const foundGenre = e.target.innerHTML;
        getFilteredBooks(foundGenre);
      });
    });
  });
}

getAllgenres();

function getAllgenres2() {
  const responsiveListGenres = document.querySelector(
    '.responsive-list-genres',
  );

  responsiveListGenres.classList.toggle('show-genres');

  axios.get(`${api}/api/books/get_all_genres.php`).then(res => {
    all_genres = res.data;
    const responsiveListGenres = document.querySelector(
      '.responsive-list-genres',
    );
    responsiveListGenres.innerHTML =
      all_genres &&
      all_genres
        .map(
          genre =>
            `
          <li class="responsive-single-genre" >${genre}</li>
        `,
        )
        .join('');
    const genres = document.querySelectorAll('.responsive-single-genre');
    genres.forEach(genre => {
      genre.addEventListener('click', function(e) {
        const foundGenre = e.target.innerHTML;
        getFilteredBooks(foundGenre);
      });
    });
  });
}

function getFilteredBooks(genre) {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const data = {
    genre: genre,
  };
  axios
    .post(`${api}/api/books/filter_by_genres.php`, config, { data })
    .then(res => {
      filtered_books = res.data;

      localStorage.removeItem('filtered_books');
      localStorage.setItem('filtered_books', JSON.stringify(filtered_books));
      window.location.replace('filtered-books.html');
    });
}

function getBookDetails(bookid) {
  const config = {
    headers: { 'Content-Type': 'application/json' },
  };
  const data = {
    book_id: bookid,
  };
  axios
    .post(`${api}/api/books/get_book_details.php`, config, { data })
    .then(res => {
      localStorage.setItem('book_details', JSON.stringify(res.data));
      window.location.replace('book-details.html');
    });
}

const account = document.querySelector('.account-link');
account.addEventListener('click', openDrawer);

const sign_out = document.querySelector('.sign-out');
sign_out.addEventListener('click', signOut);

function openDrawer() {
  const showDropDown = document.querySelector('.account-dropdown-wrapper');
  showDropDown.classList.toggle('active');
}

// responsive-account

const account2 = document.querySelector('.responsive-account-link');
account2.addEventListener('click', openDrawer2);

const sign_out2 = document.querySelector('.responsive-sign-out');
sign_out2.addEventListener('click', signOut);

function openDrawer2() {
  const showDropDown = document.querySelector(
    '.responsive-account-dropdown-wrapper',
  );
  showDropDown.classList.toggle('active');
}

window.onclick = function(event) {
  if (!event.target.matches('.account-link')) {
    const showDropDown = document.querySelector('.account-dropdown-wrapper');
    showDropDown.classList.remove('active');
  }
};

function signOut() {
  localStorage.removeItem('logged_in_user');
  localStorage.removeItem('open_chat_user');
  localStorage.removeItem('book_details');
  localStorage.removeItem('filtered_books');
  window.location.replace('login.html');
}

const searchIcon = document.querySelector('.search-icon');
const searchWrapper = document.querySelector('.search-field-wrapper');
searchIcon.addEventListener('click', function() {
  searchWrapper.classList.toggle('show-search-field');
});

const responsiveGenres = document.querySelector('.responsive-genres');
responsiveGenres.addEventListener('click', getAllgenres2);
