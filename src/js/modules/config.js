module.exports = function() {
  let api;
  if (process.env.NODE_ENV === 'prod') {
    api = process.env.PROD_API;
    return api;
  }
  if (process.env.NODE_ENV === 'dev') {
    api = 'http://localhost/book_exchange_rest_api';
    return api;
  }
};
