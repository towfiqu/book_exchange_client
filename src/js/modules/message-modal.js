import axios from 'axios';
const api = require('./config')();
const msgButton = document.getElementById('send-msg-btn');
const msgModal = document.getElementById('msg-modal');
const bookDetails = document.getElementById('single-book-details');
const navbar = document.querySelector('.nav');

msgButton.addEventListener('click', openModal);

const bookData = JSON.parse(localStorage.getItem('book_details'));
const sender = JSON.parse(localStorage.getItem('logged_in_user'));

let receiver = {};

const config = {
  headers: {
    'Content-Type': 'application/json',
  },
};

const data = {
  user_id: bookData.user_id,
};

axios
  .post(`${api}/api/users/get_user_by_id.php`, config, { data })
  .then(res => {
    receiver = res.data;
    console.log(receiver);
  });

function openModal() {
  msgModal.innerHTML = `
    <div class="msg-modal-wrapper">
      <p>Send message to ${receiver.username}</p>
      <textarea name='msg-text' id='msg-text' class='msg-field'></textarea>
      <button style="cursor:pointer;" class='msg-submit'>Send</button>
    </div>
  `;
  bookDetails.classList.add('blur-background');
  navbar.classList.add('blur-background');

  // sending message
  const submitBtn = document.querySelector('.msg-submit');
  const msgText = document.getElementById('msg-text');
  submitBtn.addEventListener('click', function() {
    sendMessage(msgText.value);
  });
}

function closeModal() {
  msgModal.innerHTML = '';
  bookDetails.classList.remove('blur-background');
  navbar.classList.remove('blur-background');
}

window.onclick = function(e) {
  if (
    !(
      e.target.matches('#send-msg-btn') ||
      e.target.matches('.msg-modal-wrapper') ||
      e.target.matches('#msg-text') ||
      e.target.matches('.msg-submit')
    )
  ) {
    closeModal();
  }
};

function sendMessage(msg) {
  const receiverName = receiver.username;
  const data = {
    msg: msg,
    username: sender.username,
    email: sender.email,
    profile_pic: sender.profile_pic,
    sender_id: sender.id,
    receiver_id: receiver.id,
  };
  axios
    .post(`${api}/api/messages/send_message.php`, config, { data })
    .then(res => {
      if (res.data.response === 'Message sent') {
        showAlert(
          `Your message has been sent to <span style="color: #333; text-transform: capitalize; font-size: 20px;">${receiverName}</span>`,
        );
      }
    });
}

function showAlert(msg) {
  msgModal.innerHTML = `
    <div class="msg-sent-wrapper">
      <p style="text-align:center; color: #00b087; font-weight: 300;">${msg}</p>
    </div>
  `;
  bookDetails.classList.add('blur-background');
  navbar.classList.add('blur-background');
}
