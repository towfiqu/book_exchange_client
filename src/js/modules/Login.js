import axios from 'axios';
const api = require('./config');
class Login {
  constructor() {
    this.events();
    this.api = api();
  }

  events() {
    const loginForm = document.getElementById('login-form');

    loginForm.onsubmit = e => {
      e.preventDefault();
      this.getUser();
    };
  }

  getUser() {
    const data = {
      email: email.value,
      password: password.value,
    };

    // console.log(data);

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    axios
      .post(
        `${this.api}/api/users/get_single_user.php`,
        config,

        { data },
      )
      .then(res => {
        if (res.data.message === 'User not found') {
          this.showError();
        } else {
          // console.log(res.data);

          localStorage.setItem('logged_in_user', JSON.stringify(res.data));
          const booksCount = JSON.parse(localStorage.getItem('logged_in_user'));
          console.log(booksCount);
          if (booksCount.user_books < 1) {
            window.location.replace('no-books.html');
          }
          if (booksCount.user_books >= 1) {
            window.location.replace('home.html');
          }
        }
      });
  }

  showError() {
    const showError = document.querySelector('.show-alert');
    showError.innerHTML = `
            <p>Inavlid email or password</p>
          `;
    setTimeout(() => {
      showError.innerHTML = '<div class="show-alert"></div>';
    }, 3000);
  }
}

const login = new Login();

// export default Login;
