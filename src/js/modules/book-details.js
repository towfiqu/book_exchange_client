const bookDetails = document.getElementById('single-book-details');
const bookDetailsPara = document.getElementById('single-book-details-p');

const book = JSON.parse(localStorage.getItem('book_details'));

bookDetails.innerHTML = `
     <img
        src=${book.cover_pic}
        alt=""
      />
      <div class="single-book-data">
        <h2>${book.title}</h2>
        <p>${book.author}</p>
        <p>${book.genre}</p>
        <a class="btn" href="library.html">View Library</a>
      </div>
      
      <button id='send-msg-btn' style="font-weight: 500;" class="btn">Contact Owner</button>

`;
bookDetailsPara.innerHTML = `
<div>
        <h2>Details</h2>
        <p>${book.book_details}</p>
      </div>
`;
